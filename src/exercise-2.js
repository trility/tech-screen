/**
 * Context:
 * A developer was asked to write a function that could retrieve a recipe with blackberries. The developer's code is below:
 *
 */

let fetch = require('fetch')
let recipes = [];

(async function(){
    let recipeIds = await fetch('http://api.allrecipes.com/recipes')

    for(let i = 0; i < recipeIds.length; i++){
        let recipe = await fetch('https://api.allrecipes.com/recipes/' + recipeIds[i])
        recipes.push(recipe)
    }
    let match;

    for(let i = 0; i < recipes.length; i++){
        if(recipes[i].ingredients.includes('blackberries')) {
            match = recipes[i]
        }
    }

    console.log(match)
})()

/**
 * It works! The function needs to be used by a service in production.  The developer requests a code review.
 *
 * Feedback:
 *
 *
 */
