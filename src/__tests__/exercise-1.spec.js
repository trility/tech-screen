/**
 * Context:
 * Joffrey and Sansa are getting married and need to send wedding invitations.  Postage is expensive in Westeros and
 * they want to minimize costs.  They have written tests to assert that the minimum amount of postage will be used.
 * They need you to finish implementing the code that will pass the tests.
 *
 */

import {
    removeDuplicates,
    groupGuests
} from '../exercise-1';

const guests = [
    'Tywin Lannister',
    'Arya Stark',
    'Ned Stark',
    'Gregor Clegane',
    'Tyrion Lannister',
    'Tyrion Lannister',
    'Ned Stark',
    'Jaime Lannister',
    'Ned Stark',
];

test('remove duplicate guest entries', () => {
    const expectedGuests = [
        'Gregor Clegane',
        'Arya Stark',
        'Ned Stark',
        'Jaime Lannister',
        'Tyrion Lannister',
        'Tywin Lannister',
    ];

    const guestList = removeDuplicates(guests);

    expect(guestList).toHaveLength(expectedGuests.length);
    expect(guestList).toContain(...expectedGuests);
});

test.skip('group by house name', () => {
    const groups = groupGuests(guests);

    expect(groups['Stark'].sort()).toEqual(['Arya', 'Ned']);
    expect(groups['Lannister'].sort()).toEqual(['Jaime', 'Tyrion', 'Tywin']);
    expect(groups['Clegane']).toEqual(['Gregor']);
});

test.skip('send invites and receive (asynchronous) headcount', () => {
    let headcount = 0;

    //TODO: use sendInvitation method from exercise-1

    expect(headcount).toEqual(5);
});
